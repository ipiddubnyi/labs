package lab2.controller;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;
import lab2.bean.Car;
import lab2.bean.Pagination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;

@Controller("/cars")
public class CarController {

    private static final Logger LOG = LoggerFactory.getLogger(CarController.class);

    @Get("/{?pagination}")
    public Car list(@Nullable Pagination pagination) {
        LOG.info("Paging {} ", pagination);
        return new Car();
    }
}
