package lab2.bean;

public class Pagination {

    private Integer page;
    private Integer pageSize;

    public void setPage(Integer page) {
        this.page = page;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Pagination{" +
            "page=" + page +
            ", pageSize=" + pageSize +
            '}';
    }
}
